/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavideojuegos;

import java.util.ArrayList;

/**
 *
 * @author Raúl
 */
public class Juego extends Plataforma{
    
    protected int id_juego = 0;
    protected String titulo = "";
    protected String genero = "";
            
    public Juego(int id, String titulo, String genero, Plataforma plataforma) {
        super(plataforma.id_plataforma, plataforma.nombre_plataforma);
        this.id_juego = id;
        this.titulo = titulo;
        this.genero = genero;
    }

    public int getId_juego() {
        return id_juego;
    }

    public void setId_juego(int id_juego) {
        this.id_juego = id_juego;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    public String getPlataforma(){
        return this.nombre_plataforma;
    }
    
    @Override
    public String toString() {
        return "Juego: {" + "id_juego = " + id_juego + ", Título = " + titulo + ", Genero = " + genero + ", Plataforma = " + nombre_plataforma + '}';
    }
    
}
