/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavideojuegos;

import java.util.ArrayList;

/**
 *
 * @author Raúl
 */
public class Jugador{
    
    protected String DNI = "";
    protected String nombre = "";
    protected String apellidos = "";
    public ArrayList<Juego> juegos = new ArrayList<Juego>();
            
    public Jugador(String DNI, String nombre, String apellidos) {
        this.DNI = DNI;
        this.nombre = nombre;
        this.apellidos = apellidos;
    }

    public String getDNI() {
        return DNI;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setJuego(Juego juego) {
        juegos.add(juego);
    }
    
    public ArrayList<Juego> getJuegos(){
        return this.juegos;
    }
    
    @Override
    public String toString() {
        return "Jugador:{" + "DNI = " + DNI + ", nombre = " + nombre + ", apellidos = " + apellidos + "}";
    }
    
}
