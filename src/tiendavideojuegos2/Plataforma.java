/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavideojuegos;

import java.util.ArrayList;

/**
 *
 * @author Raúl
 */
public class Plataforma {
    protected int id_plataforma = 0;
    protected String nombre_plataforma = "";
    
    public Plataforma(int id, String nombre){
        this.id_plataforma = id;
        this.nombre_plataforma = nombre;
    }

    public int getId_plataforma() {
        return id_plataforma;
    }

    public void setId_plataforma(int id_plataforma) {
        this.id_plataforma = id_plataforma;
    }

    public String getNombre_plataforma() {
        return nombre_plataforma;
    }

    public void setNombre_plataforma(String nombre_plataforma) {
        this.nombre_plataforma = nombre_plataforma;
    }

    @Override
    public String toString() {
        return "Plataforma: {" + "id_plataforma = " + id_plataforma + ", Nombre = " + nombre_plataforma + '}';
    }
    
}
