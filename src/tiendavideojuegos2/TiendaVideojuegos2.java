/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendavideojuegos2;
import com.db4o.*;
import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import tiendavideojuegos.Juego;
import tiendavideojuegos.Jugador;
import tiendavideojuegos.Plataforma;

/**
 *
 * @author Raúl
 */
public class TiendaVideojuegos2 {

    private ArrayList<tiendavideojuegos.Plataforma> plataformas = new ArrayList<tiendavideojuegos.Plataforma>();
    private ArrayList<tiendavideojuegos.Juego> juegos = new ArrayList<tiendavideojuegos.Juego>();
    private ArrayList<tiendavideojuegos.Jugador> jugadores = new ArrayList<tiendavideojuegos.Jugador>();
    private String identificador = "tienda_store";
    
    public TiendaVideojuegos2(){    
    }
    
    public TiendaVideojuegos2(ArrayList<Plataforma> plataformas, ArrayList<Juego> juegos, ArrayList<Jugador> jugadores, String ifentificador){
        this.plataformas = plataformas;
        this.juegos = juegos;
        this.jugadores = jugadores;
        this.identificador = identificador;
    }
    
    /*
     * Constructor inicializado
     */
    public TiendaVideojuegos2(boolean inicializacion){
        inicializarPlataformas();
        inicializarJuegos();
        inicializarJugadores();
    }
    
    /**********INICIALIZADORES***************/
    public void inicializarPlataformas(){
        plataformas.add(new tiendavideojuegos.Plataforma(0, "NES"));
        plataformas.add(new tiendavideojuegos.Plataforma(1, "3DS"));
        plataformas.add(new tiendavideojuegos.Plataforma(2, "Wii"));
        plataformas.add(new tiendavideojuegos.Plataforma(3, "Wii U"));
        plataformas.add(new tiendavideojuegos.Plataforma(4, "Switch"));
        plataformas.add(new tiendavideojuegos.Plataforma(5, "PS2"));
        plataformas.add(new tiendavideojuegos.Plataforma(6, "PS3"));
        plataformas.add(new tiendavideojuegos.Plataforma(7, "PS4"));
        plataformas.add(new tiendavideojuegos.Plataforma(8, "XBOX 360"));
        plataformas.add(new tiendavideojuegos.Plataforma(9, "XBOX ONE"));
        plataformas.add(new tiendavideojuegos.Plataforma(10, "PC"));
    }
    
    public void inicializarJuegos(){
        juegos.add(new tiendavideojuegos.Juego(0, "Grand Theft Auto: V", "RPG", plataformas.get(6)));
        juegos.add(new tiendavideojuegos.Juego(1, "Grand Theft Auto: V", "RPG", plataformas.get(7)));
        juegos.add(new tiendavideojuegos.Juego(2, "Grand Theft Auto: V", "RPG", plataformas.get(8)));
        juegos.add(new tiendavideojuegos.Juego(3, "Grand Theft Auto: V", "RPG", plataformas.get(9)));
        juegos.add(new tiendavideojuegos.Juego(4, "Grand Theft Auto: V", "RPG", plataformas.get(10)));
        juegos.add(new tiendavideojuegos.Juego(5, "The Last Guardian", "Aventura, Puzzles", plataformas.get(7)));
        juegos.add(new tiendavideojuegos.Juego(6, "The Vegeteables Returns: The Game", "Aventura, Rol", plataformas.get(1)));
    }
    
    public void inicializarJugadores(){
        jugadores.add(new tiendavideojuegos.Jugador("111111A", "Raúl", "Labrat Rodríguez"));
        jugadores.get(0).setJuego(juegos.get(4));
        jugadores.get(0).setJuego(juegos.get(5));
        jugadores.add(new tiendavideojuegos.Jugador("222222B", "Nacho", "Er mah Locohh"));
        jugadores.get(1).setJuego(juegos.get(2));
        jugadores.add(new tiendavideojuegos.Jugador("333333C", "Lolillo", "VeganBOY"));
        jugadores.get(2).setJuego(juegos.get(6));
    }
    
    /**************OPCIONES DEL MENÚ*************/
    public void imprimirMenu(){
        System.out.println("¡Bienvenido a nuestra tienda de videojuegos!");
        System.out.println("Escriba \"salir\", para salir del programa.");
        System.out.println("Escriba \"ayuda\", para ver las opciones del menu en cualquier momento.");
        System.out.println("Escriba \"nuevo\", para añadir algo al sistema.");
        System.out.println("Escriba \"registrarse\", para registrarse en el sistema como jugador.");
        System.out.println("Escriba \"registrar juego\", para registrar un juego en su cuenta.");
        System.out.println("Escriba \"consultar\", para ver los distintos contenidos de la tienda.");
        System.out.println("Escriba \"modificar\", para acceder al submenú de las modificaciones.");
        System.out.println("Escriba \"eliminar\", para acceder al submenú de eliminaciones.");
    }
    
    /*************CONSULTAS DEL CRUD*****************/
    public void imprimirJugadores(){
        for(int i=0; i<jugadores.size();i++){
            System.out.println(jugadores.get(i).toString());
        }
    }
    
    public void imprimirJuegos(){
        for(int i=0; i<juegos.size();i++){
            System.out.println(juegos.get(i).toString());
        }
    }
    
    public void imprimirPlataformas(){
        for(int i=0; i<plataformas.size();i++){
            System.out.println(plataformas.get(i).toString());
        }
    }
    
    public void imprimirJugadoresYJuegos(){
        for(int i=0; i<this.jugadores.size();i++){
            System.out.println(this.jugadores.get(i).toString());
            for(int j=0; j<this.jugadores.get(i).juegos.size();j++){
                System.out.println("   " + this.jugadores.get(i).juegos.get(j).toString());
            }
        }
    }
    
    public void imprimirJuegosDelJugador(tiendavideojuegos.Jugador jugador){
        System.out.println(jugador.toString());
        for(int i=0; i<jugadores.size(); i++){
            if(jugadores.get(i).getDNI().equals(jugador.getDNI())){
               for(int j=0; j<jugadores.get(i).juegos.size(); j++){
                   System.out.println(jugadores.get(i).juegos.get(j).toString());
               }
            }
        }
    }

    public void imprimirJugadoresPorTitulo(tiendavideojuegos.Juego juego){
        for(int i=0; i<jugadores.size(); i++){
            for(int j=0; j<jugadores.get(i).juegos.size(); j++){
                if(jugadores.get(i).juegos.get(j).getTitulo().equals(juego.getTitulo())){
                    System.out.println(jugadores.get(i).toString());
                }
            }
        }
    }
   
    public void imprimirJugadoresPorPlataforma(tiendavideojuegos.Plataforma plataforma){
        for(int i=0; i<jugadores.size(); i++){
            for(int j=0; j<jugadores.get(i).juegos.size(); j++){
                if(jugadores.get(i).juegos.get(j).getPlataforma().equals(plataforma.getNombre_plataforma())){
                   System.out.println(jugadores.get(i).toString());
                }
            }
        }
    }
    
    public void imprimirJuegosPorPlataforma(tiendavideojuegos.Plataforma plataforma){
        for(int i=0; i<juegos.size(); i++){
            if(juegos.get(i).getPlataforma().equals(plataforma.getNombre_plataforma())){
                System.out.println("    " + juegos.get(i).toString());
            }
        }
    }
    
    public void imprimirJuegosYPlataformas(){
        for(int i=0; i<this.plataformas.size(); i++){
            System.out.println(this.plataformas.get(i).toString());
            this.imprimirJuegosPorPlataforma(this.plataformas.get(i));
        }
    }
    
    public void imprimirPlataformasPorJuegos(tiendavideojuegos.Juego juego){
        System.out.println(juego.getTitulo());
        for(int i=0; i<juegos.size(); i++){
            if(juegos.get(i).getTitulo().equals(juego.getTitulo())){
                for(int j=0; j<plataformas.size(); j++){
                    if(juegos.get(i).getId_plataforma() == plataformas.get(j).getId_plataforma()){
                        System.out.println(plataformas.get(j).toString());
                    }                
                }
            }
        }
        System.out.println("\n");
    }
    
    /****************BUSQUEDAS*********************/
    public int buscarJugador(String DNI){
        int i = 0;
        boolean encontrado = false;
        for (i=0; i<jugadores.size() && encontrado == false; i++){
            if(jugadores.get(i).getDNI().equals(DNI)){
                encontrado = true;
            }
        }
        if(encontrado == true)
           return i-1; 
        else
            return -1;
    }
    
    public int buscarJuego(String titulo){
        int i = 0;
        boolean encontrado = false;
        for (i=0; i<juegos.size() && encontrado == false; i++){
            if(juegos.get(i).getTitulo().equals(titulo)){
                encontrado = true;
            }
        }
        if(encontrado == true)
           return i-1; 
        else
            return -1;
    }
    
    public int buscarJuego(String titulo, String plataforma){
        int i = 0;
        boolean encontrado = false;
        for (i=0; i<juegos.size() && encontrado == false; i++){
            if(juegos.get(i).getTitulo().equals(titulo) && juegos.get(i).getPlataforma().equals(plataforma)){
                encontrado = true;
            }
        }
        if(encontrado == true)
           return i-1; 
        else
            return -1;
    }
    
    public int buscarPlataforma(String nombre){
        int i = 0;
        boolean encontrado = false;
        for (i=0; i<plataformas.size() && encontrado == false; i++){
            if(plataformas.get(i).getNombre_plataforma().equals(nombre)){
                encontrado = true;
            }
        }
        if(encontrado == true)
           return i-1; 
        else
            return -1;
    }
    
    /****************ALMACENAMIENTO DEL PROGRAMA******************/
    //Guardado
    public static void guardarFichero (TiendaVideojuegos2 tienda, ObjectContainer db) throws FileNotFoundException{
        db.store(tienda);
    }
    
    //Lectura
    public static TiendaVideojuegos2 leerFichero (ObjectContainer db) throws FileNotFoundException {
            TiendaVideojuegos2 tienda = new TiendaVideojuegos2(null, null, null, "tienda_store");
            TiendaVideojuegos2 tiendaRes = null;
            ObjectSet<TiendaVideojuegos2> result = db.queryByExample(tienda);
            if(result.isEmpty()){
                System.out.println("No se ha encontrado la tienda en la base de datos");
            }
            else{
                while(result.hasNext()){
                    tiendaRes = result.next();
                }
            }
            
            return tiendaRes;
    }
    
    /***************ELIMINADORES****************/
    public void eliminarJugador(){
        Scanner teclado = new Scanner(System.in);
        int posicion_jugador = 0;
        
        System.out.println("Va a eliminar a un jugador del sistema.\nPor favor introduzca el DNI del jugador a eliminar: ");
        String DNI = teclado.nextLine();
        
        posicion_jugador = this.buscarJugador(DNI);
        while(posicion_jugador == -1){
            System.out.println("No hay ningún jugador con el DNI introducido.\nPor favor vuelva a introducir el DNI: ");
            DNI = teclado.nextLine();
            posicion_jugador = this.buscarJugador(DNI);
        }
        
        this.jugadores.remove(posicion_jugador);
    }
    
    public void eliminarJuego(){
        Scanner teclado = new Scanner(System.in);
        int posicion_juego = 0;
        
        System.out.println("Va a eliminar un juego del sistema.\nPor favor introduzca el titulo del juego a eliminar: ");
        String titulo = teclado.nextLine();
        System.out.println("Ahora introduzca la plataforma: ");
        String plataforma = teclado.nextLine();
        
        posicion_juego = this.buscarJuego(titulo, plataforma);
        while(posicion_juego == -1){
            System.out.println("No hay ningún juego con el titulo y/o plataforma introducidos.\nPor favor vuelva a introducir el titulo: ");
            titulo = teclado.nextLine();
            System.out.println("Ahora vuelva a introducir la plataforma: ");
            plataforma = teclado.nextLine();
            posicion_juego = this.buscarJuego(titulo, plataforma);
        }
        
        for(int i=0; i<this.jugadores.size();i++){
            for(int j=0; j<this.jugadores.get(i).juegos.size();j++){
                if(this.jugadores.get(i).juegos.get(j).getTitulo().equals(this.juegos.get(posicion_juego).getTitulo()) && this.jugadores.get(i).juegos.get(j).getPlataforma().equals(this.juegos.get(posicion_juego).getPlataforma())){
                    this.jugadores.get(i).juegos.remove(j);
                }
            }
        }
        
        this.juegos.remove(posicion_juego);
    }
    
    public void eliminarPlataforma(){
        Scanner teclado = new Scanner(System.in);
        int posicion_plataforma = 0;
        
        System.out.println("Va a eliminar una plataforma del sistema.\nPor favor introduzca el nombre de la plataforma que desea eliminar: ");
        String nombre = teclado.nextLine();
        
        posicion_plataforma = this.buscarPlataforma(nombre);
        while(posicion_plataforma == -1){
            System.out.println("No existe ninguna plataforma con el nombre introducido.\nPor favor introduzca el nombre de nuevo: ");
            nombre = teclado.nextLine();
            posicion_plataforma = this.buscarPlataforma(nombre);
        }
        
        for(int i=0; i<this.jugadores.size();i++){
            for(int j=0; j<this.jugadores.get(i).juegos.size();j++){
                if(this.jugadores.get(i).juegos.get(j).getPlataforma().equals(this.plataformas.get(posicion_plataforma).getNombre_plataforma())){
                    this.jugadores.get(i).juegos.remove(j);
                }
            }
        }
        
        for(int i=0; i<this.juegos.size();i++){
            if(this.juegos.get(i).getPlataforma().equals(this.plataformas.get(posicion_plataforma).getNombre_plataforma())){
                this.juegos.remove(i);
            }
        }
        
        this.plataformas.remove(posicion_plataforma);
    }
    
    /***************MODIFICADORES****************/
    public void modificarJugador(){
        Scanner teclado = new Scanner(System.in);
        int posicion_jugador = 0;
        
        System.out.println("Va ha modificar un jugador del sistema.\nPor favor, introduzca el DNI del jugador a modificar: ");
        String DNI = teclado.nextLine();
        
        posicion_jugador = this.buscarJugador(DNI);
        while(posicion_jugador == -1){
            System.out.println("No se ha encontrado a ningún jugador registrado con el DNI introducido.\nPor favor introduzca el DNI de nuevo: ");
            DNI = teclado.nextLine();
            posicion_jugador = this.buscarJugador(DNI);
        }
        
        System.out.println("Introduzca los valores correspondientes según se vayan pidiendo(Si desea mantener algún valor como el original, simplemente deje vacío el campo.)");
        System.out.println("\nIntroduzca el DNI: ");
        String nuevo_DNI = teclado.nextLine();
        if(nuevo_DNI.equals("")){
            nuevo_DNI = this.jugadores.get(posicion_jugador).getDNI();
        }
        System.out.println("Ahora, introduzca el nombre: ");
        String nuevo_nombre = teclado.nextLine();
        if(nuevo_nombre.equals("")){
            nuevo_nombre = this.jugadores.get(posicion_jugador).getNombre();
        }
        System.out.println("Por último, introduzca los apellidos: ");
        String nuevos_apellidos = teclado.nextLine();
        if(nuevos_apellidos.equals("")){
            nuevos_apellidos = this.jugadores.get(posicion_jugador).getApellidos();
        }
        
        ArrayList<tiendavideojuegos.Juego> juegos = new ArrayList<tiendavideojuegos.Juego>();
        juegos = this.jugadores.get(posicion_jugador).getJuegos();
        
        this.jugadores.set(posicion_jugador, new tiendavideojuegos.Jugador(nuevo_DNI, nuevo_nombre, nuevos_apellidos));
        for(int i=0; i<juegos.size(); i++){
            this.jugadores.get(posicion_jugador).setJuego(juegos.get(i));
        }
    }
    
    public void modificarJuego(){
        Scanner teclado = new Scanner(System.in);
        int posicion_juego = 0;
        int posicion_plataforma = 0;
        
        System.out.println("Va ha modificar un juego del sistema.\nPor favor, introduzca el titulo del juego a modificar: ");
        String titulo = teclado.nextLine();
        System.out.println("Ahora, introduzca la plataforma: ");
        String plataforma = teclado.nextLine();
        
        posicion_juego = this.buscarJuego(titulo, plataforma);
        while(posicion_juego == -1){
            System.out.println("El titulo introducido y/o la plataforma no se encuentra en el sistema.\nPor favor pruebe a introducir de nuevo el titulo: ");
            titulo = teclado.nextLine();
            System.out.println("Ahora introduzca de nuevo la plataforma: ");
            plataforma = teclado.nextLine();
            posicion_juego = this.buscarJuego(titulo, plataforma);
        }
        
        System.out.println("Introduzca los valores correspondientes según se vayan pidiendo(Si desea mantener algún valor como el original, simplemente deje vacío el campo.)");
        System.out.println("\nIntroduzca el nuevo título: ");
        String nuevo_titulo = teclado.nextLine();
        if(nuevo_titulo.equals("")){
            nuevo_titulo = this.juegos.get(posicion_juego).getTitulo();
        }
        System.out.println("Ahora introduzca el nuevo genero: ");
        String nuevo_genero = teclado.nextLine();
        if(nuevo_genero.equals("")){
            nuevo_genero = this.juegos.get(posicion_juego).getGenero();
        }
        System.out.println("Por último introduzca la plataforma: ");
        String nueva_plataforma = teclado.nextLine();
        if(nueva_plataforma.equals("")){
            nueva_plataforma = this.juegos.get(posicion_juego).getPlataforma();
        }
        
        posicion_plataforma = this.buscarPlataforma(nueva_plataforma);
        while(posicion_plataforma == -1){
            System.out.println("No se ha encontrado la plataforma introducida.\nPor favor introduzcala de nuevo: ");
            nueva_plataforma = teclado.nextLine();
            posicion_plataforma = this.buscarPlataforma(nueva_plataforma);
        }
        
        this.juegos.set(posicion_juego, new tiendavideojuegos.Juego(this.juegos.get(posicion_juego).getId_juego(), nuevo_titulo, nuevo_genero, this.plataformas.get(posicion_plataforma)));
    }
    
    /***************AÑADIDOS**************/
    public void anadirJuego(){
        Scanner teclado = new Scanner(System.in);
        int posicion_plataforma = 0;
        
        System.out.println("Va a añadir un nuevo juego al sistema.\nPor favor, introduzca el titulo: ");
        String titulo = teclado.nextLine();
        System.out.println("Ahora introduzca el genero: ");
        String genero = teclado.nextLine();
        System.out.println("Ahora introduzca la plataforma: ");
        String plataforma = teclado.nextLine();
        
        posicion_plataforma = this.buscarPlataforma(plataforma);
        while(posicion_plataforma == -1){
            System.out.println("No se encuentra la plataforma introducida.\nPor favor pruebe de nuevo: ");
            plataforma = teclado.nextLine();
            posicion_plataforma = this.buscarPlataforma(plataforma);
        }
        
        this.juegos.add(new tiendavideojuegos.Juego(this.juegos.size(), titulo, genero, this.plataformas.get(posicion_plataforma)));
    }
    
    public void anadirJuegoAJugador(){
        Scanner teclado = new Scanner(System.in);
        int posicion_juego = 0;
        int posicion_jugador = 0;
        
        System.out.println("Va a añadir un nuevo juego a su cuenta.\nPor favor, introduzca el título del juego que quiere añadir: ");
        this.imprimirJuegos();
        String titulo = teclado.nextLine();
        System.out.println("Introduzca la plataforma(Opcional, si no la quiere introducir deje en blanco el hueco): ");
        this.imprimirPlataformas();
        String plataforma = teclado.nextLine();
        
        if(plataforma.equals("")){
            posicion_juego = this.buscarJuego(titulo);
        }
        else{
            posicion_juego = this.buscarJuego(titulo, plataforma);
        }
        while(posicion_juego == -1){
            System.out.println("No se encuentra el juego introducido.\nPor favor, vuelva a introducir el titulo: ");
            titulo = teclado.nextLine();
            System.out.println("y/o la plataforma(recuerde deje en blanco para omitir): ");
            plataforma = teclado.nextLine();
            if(plataforma.equals("")){
                posicion_juego = this.buscarJuego(titulo);
            }
            else{
                posicion_juego = this.buscarJuego(titulo, plataforma);
            }
        }
        
        System.out.println("Ahora, por favor introduzca su DNI: ");
        String DNI = teclado.nextLine();
        
        posicion_jugador = this.buscarJugador(DNI);
        while(posicion_jugador == -1){
            System.out.println("No se ha encontrado a ningún jugador registrado con ese DNI.\nPor favor, vuelva a introducir el DNI: ");
            DNI = teclado.nextLine();
            posicion_jugador = this.buscarJugador(DNI);
        }
        
        this.jugadores.get(posicion_jugador).setJuego(this.juegos.get(posicion_juego));        
    }
    
    public void anadirPlataforma(){
        Scanner teclado = new Scanner(System.in);
        System.out.println("Va a introducir una nueva plataforma en el sistema.\nPor favor, introduzca el nombre de la plataforma: ");
        String nombre = teclado.nextLine();
        
        this.plataformas.add(new tiendavideojuegos.Plataforma(this.plataformas.size(), nombre));
    }
    
    public void anadirJugador(){
        Scanner teclado = new Scanner(System.in);
        System.out.println("Va a registrarse como un nuevo jugador en esta tienda.\nPor favor, introduzca su nombre: ");
        String nombre = teclado.nextLine();
        System.out.println("Ahora introduzca sus apellidos: ");
        String apellidos = teclado.nextLine();
        System.out.println("Ahora introduzca su DNI: ");
        String DNI = teclado.nextLine();
        this.jugadores.add(new tiendavideojuegos.Jugador(DNI, nombre, apellidos));
        
        System.out.println("Enhorabuena ya eres miembro de esta tienda.");
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        // TODO code application logic here
        boolean inicializado = true;
        int salida = 0;
        String BDdir = "D:\\OneDrive\\Clases\\2_DAM\\AD\\NetBeansProyects\\TiendaVideojuegos2\\DBTiendaVideojuegos.yap";
        ObjectContainer db = null;
        TiendaVideojuegos2 fnac;
        File fichero = new File("DBTiendaVideojuegos.yap");
        
        /*********COMPROBACIÓN DEL FICHERO*********/
        if(fichero.exists()){
            System.out.println("Base de datos encontrada.");
            fnac = new TiendaVideojuegos2();
            db = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(),BDdir);
            fnac = leerFichero(db);
            System.out.println("\nBase de datos cargada con éxito.");
        }
        else{
            System.out.println("No hay ningúna base de datos creada anteriormente, se procederá a su creación.");
            fnac = new TiendaVideojuegos2(inicializado);
            db = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(),BDdir);
            guardarFichero(fnac, db);
            System.out.println("Base de datos creada y guardada correctamente.");
        }
        
       fnac.imprimirMenu();
        
        /**************MENÚ*************/
        while(salida != -1){
            Scanner teclado = new Scanner(System.in);
            switch(teclado.nextLine()){
                case "Salir":
                case "salir":   salida = -1;
                                break;
                case "Nuevo":
                case "nuevo":  System.out.println("Introduza \"plataforma\", para añadir una plataforma.\nIntroduzca \"juego\", para añadir un juego: ");
                                switch(teclado.nextLine()){
                                    case "Plataforma":
                                    case "plataforma":  fnac.imprimirPlataformas();
                                                        fnac.anadirPlataforma();
                                                        guardarFichero(fnac, db);
                                                        System.out.println("Plataforma añadida con éxito.");
                                                        break;
                                    case "Juego":
                                    case "juego":       fnac.imprimirJuegos();
                                                        fnac.anadirJuego();
                                                        guardarFichero(fnac, db);
                                                        System.out.println("Juego añadido con éxito.");
                                                        break;
                                    default:    System.out.println("Ha ocurrido un error, volviendo al menú prinicpal.");
                                }
                                break;
                case "Registrarse":
                case "registrarse":     fnac.imprimirJugadores();
                                        fnac.anadirJugador();
                                        guardarFichero(fnac, db);
                                        break;
                case "Registrar Juego":
                case "registrar juego":     fnac.imprimirJuegos();
                                            fnac.anadirJuegoAJugador();
                                            guardarFichero(fnac, db);
                                            System.out.println("Juego añadido con éxito.");
                                            break;
                case "Consultar":   
                case "consultar":   System.out.println("Introduzca \"juegos\", para acceder a las consultas relacionadas con los juegos.");
                                    System.out.println("Introduzca \"plataformas\", para mostrar todas las plataformas.");
                                    System.out.println("Introduzca \"jugadores\", para mostrar todos los jugadores.");
                                    switch(teclado.nextLine()){
                                        case "Juegos":
                                        case "juegos":  System.out.println("Introduzca \"por plataforma\", para mostrar los juegos por sus plataformas.");
                                                        System.out.println("Introduzca \"de los usuarios\", para mostrar los juegos de los usuarios.");
                                                        System.out.println("Introduzca \"todos\", para mostrar todos los juegos.");
                                                        switch(teclado.nextLine()){
                                                            case "Por Plataforma":
                                                            case "por plataforma":  fnac.imprimirJuegosYPlataformas();
                                                                                    break;
                                                            case "De Los Usuarios":
                                                            case "de los usuarios": fnac.imprimirJugadoresYJuegos();
                                                                                    break;
                                                            case "Todos": 
                                                            case "todos":   fnac.imprimirJuegos();
                                                                            break;
                                                            default:    System.out.println("Ha ocurrido un error, volviendo al menú prinicpal.");
                                                        }
                                                        break;
                                        case "Plataformas":
                                        case "plataformas": fnac.imprimirPlataformas();
                                                            break;
                                        case "Jugadores":
                                        case "jugadores":   fnac.imprimirJugadores();
                                                            break;
                                        default:    System.out.println("Ha ocurrido un error, volviendo al menú prinicpal.");
                                    }
                                    break;
                case "Modificar":
                case "modificar":   System.out.println("Introduzca \"jugador\", para modificar un jugador.");
                                    System.out.println("Introduzca \"juego\", para modificar un juego.");
                                    switch(teclado.nextLine()){
                                        case "Jugador":
                                        case "jugador": fnac.imprimirJugadores();
                                                        fnac.modificarJugador();
                                                        guardarFichero(fnac, db);
                                                        System.out.println("Jugador modificado con éxito.");
                                                        break;
                                        case "Juego":
                                        case "juego":   fnac.imprimirJuegos();
                                                        fnac.modificarJuego();
                                                        guardarFichero(fnac, db);
                                                        System.out.println("Juego modificado con éxito.");
                                                        break;
                                        default:    System.out.println("Ha ocurrido un error, volviendo al menú prinicpal.");
                                    }
                                    break;
                case "Eliminar":
                case "eliminar":    System.out.println("Introduzca \"plataforma\", para eliminar una plataforma.");
                                    System.out.println("Introduzca \"juego\", para eliminar un juego.");
                                    System.out.println("Introduzca \"jugador\", para eliminar un jugador.");
                                    switch(teclado.nextLine()){
                                        case "Plataforma": 
                                        case "plataforma":  fnac.imprimirPlataformas();
                                                            fnac.eliminarPlataforma();
                                                            guardarFichero(fnac, db);
                                                            System.out.println("Plataforma eliminada con éxito.");
                                                            break;
                                        case "Juego":
                                        case "juego":   fnac.imprimirJuegos();
                                                        fnac.eliminarJuego();
                                                        guardarFichero(fnac, db);
                                                        System.out.println("Juego eliminado con éxito.");
                                                        break;
                                        case "Jugador":
                                        case "jugador": fnac.imprimirJugadores();
                                                        fnac.eliminarJugador();
                                                        guardarFichero(fnac, db);
                                                        System.out.println("Jugador eliminado con éxito");
                                                        break;
                                        default:    System.out.println("Ha ocurrido un error, volviendo al menú prinicpal.");
                                    }
                                    break;
                case "Ayuda":
                case "ayuda":   fnac.imprimirMenu();
                                break;
                                
                default: 
                    System.out.println("Ha ocurrido un error, vuelva a intentarlo.");
            }
        }
        
        guardarFichero(fnac, db);
        db.close();
    }
    
}
